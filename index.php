<?php
	require_once 'animal.php';
	require_once 'ape.php';
	require_once 'frog.php';

	echo "<h2>Release 0</h2>";

	$sheep = new Animal("shaun");

	echo "Animal Name: " . $sheep->name . "<br>"; // "shaun"
	echo "Animal Legs: " . $sheep->legs . "<br>"; // 2
	echo "Cold Blooded: " . $sheep->cold_blooded . "<br>"; // false


	echo "<h2>Release 1</h2>";

	$sungokong = new Ape("kera sakti");

	echo "Animal Name: " . $sungokong->name . "<br>"; // "kera sakti"
	$sungokong->yell(); // "Auooo"

	
	$kodok = new Frog("buduk");

	echo "Animal Name: " . $kodok->name . "<br>"; // "buduk"
	$kodok->jump() ; // "hop hop"
?>