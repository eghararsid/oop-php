<?php
	require_once 'animal.php';

	class Ape extends Animal {
		public function yell() {
			echo "Animal Legs: " . $this->legs . "<br>"; // 2
			echo "Cold Blooded: " . $this->cold_blooded . "<br>"; // false
			echo "Yelling Sound: Auooo<br><br>";
		}
	}
?>