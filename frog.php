<?php
	require_once 'animal.php';

	class Frog extends Animal {
		public function jump() {
			echo "Animal Legs: " . $this->legs = 4 . "<br>"; // 2
			echo "Cold Blooded: " . $this->cold_blooded . "<br>"; // false	
			echo "Jumping Sound: hop hop";
		}
	}
?>